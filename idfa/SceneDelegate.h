//
//  SceneDelegate.h
//  idfa
//
//  Created by Валерий on 25.08.2020.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

