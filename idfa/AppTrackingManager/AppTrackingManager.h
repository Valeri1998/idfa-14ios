#import <Foundation/Foundation.h>

//Не забываем добавить в Info.plist NSUserTrackingUsageDescription
@interface AppTrackingManager: NSObject

+ (instancetype)sharedInstance;

@property (strong, nonatomic, readonly) NSString *advertisingIdentifier;
@property (nonatomic, readonly) BOOL trackingIsAllowed API_AVAILABLE(ios(14.0));
- (void)sendAppTrackingRequest API_AVAILABLE(ios(14.0));

@end
