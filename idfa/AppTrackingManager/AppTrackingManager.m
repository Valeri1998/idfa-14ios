#import "AppTrackingManager.h"
#import <AdSupport/ASIdentifierManager.h>
#import <AppTrackingTransparency/AppTrackingTransparency.h>

@implementation AppTrackingManager
+ (instancetype)sharedInstance {
    static id _sharedInstance;
    if(!_sharedInstance) {
        static dispatch_once_t oncePredicate;
        
        dispatch_once(&oncePredicate, ^{
            _sharedInstance = [self new];
        });
    }
    
    return _sharedInstance;
}

- (NSString *)advertisingIdentifier {
    return [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
}

- (BOOL)trackingIsAllowed {
    return ATTrackingManager.trackingAuthorizationStatus == ATTrackingManagerAuthorizationStatusAuthorized;
}

- (void)sendAppTrackingRequest {
    __weak __typeof(&*self)weakSelf = self;
    
    [ATTrackingManager requestTrackingAuthorizationWithCompletionHandler:^(ATTrackingManagerAuthorizationStatus status) {
        [weakSelf handleChangingAppTrackingStatusWith:status];
    }];
}

#pragma mark - Private -
- (void)handleChangingAppTrackingStatusWith:(ATTrackingManagerAuthorizationStatus)status {
    switch (status) {
        case ATTrackingManagerAuthorizationStatusNotDetermined: //пользователь еще не получил запрос на авторизацию
            NSLog(@"ATTrackingManagerAuthorizationStatusNotDetermined");
//                UnitySendMessage("AdvertisingIdRequestHandler", "HandleSuccess", NO);
            break;
        case ATTrackingManagerAuthorizationStatusAuthorized: //пользователь разрешает доступ к данным
            NSLog(@"ATTrackingManagerAuthorizationStatusAuthorized");
//                UnitySendMessage("AdvertisingIdRequestHandler", "HandleSuccess", YES);
            break;
        case ATTrackingManagerAuthorizationStatusRestricted: //авторизация для доступа к данным ограничена
            NSLog(@"ATTrackingManagerAuthorizationStatusRestricted");
//                UnitySendMessage("AdvertisingIdRequestHandler", "HandleSuccess", NO);
            break;
        case ATTrackingManagerAuthorizationStatusDenied: //пользователь отказывает в авторизации для доступа
            NSLog(@"ATTrackingManagerAuthorizationStatusDenied");
//                UnitySendMessage("AdvertisingIdRequestHandler", "HandleSuccess", NO);
            //Отсюда нужно показать алерт, который перекинет пользователя в настройки для включения слежки
            break;
    }
}

@end
