#import "ViewController.h"
#import "AppTrackingManager.h"

@implementation ViewController

- (IBAction)doGetIDFA:(UIButton *)sender {
    [AppTrackingManager.sharedInstance sendAppTrackingRequest];
    
    //Как то так должно выглядеть действие обновления статуса
    if (AppTrackingManager.sharedInstance.trackingIsAllowed) {
        NSLog(@"App tracking is allowed. IDFA: %@", AppTrackingManager.sharedInstance.advertisingIdentifier);
    }
    else {
        NSLog(@"App tracking not allowed");
    }
}

@end
